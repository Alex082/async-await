/**
1. Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

   виполнение части кода без ожидания окончания, перед следующим выполнением
 */



//вешаем слик на кноркку
const button = document.querySelector(".btn");
button.addEventListener("click", findLocation);


// запросы на ссылки
async function findLocation() {
  try {
    const { ip } = await (await fetch("http://api.ipify.org/?format=json")).json();
    console.log(ip);
    const locationData = await (await fetch(`http://ip-api.com/json/${ip}?fields=status,message,continent,country,countryCode,region,regionName,city,district,zip,lat,lon,timezone,isp,org,as,query`)).json();
    console.log(locationData);
    displayLocationData(locationData);
  } catch (error) {
    console.log("Помилка:", error);
  }
}


//вывод
function displayLocationData(data) {
  const locationContainer = document.querySelector(".location-container");
  locationContainer.innerHTML = `
    <p>Континент: ${data.continent}</p>
    <p>Країна: ${data.country}</p>
    <p>Регіон: ${data.region}</p>
    <p>Місто: ${data.city}</p>
    <p>Район: ${data.district}</p>
  `;
}
